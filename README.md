
# QGIS Census Downloader Plugin

A user-friendly QGIS plugin that simplifies the process of downloading and combining census data with geographic boundaries from the US Census Bureau's APIs. The plugin provides an intuitive graphical interface for accessing demographic data and TIGER/Line shapefiles, making it easier to perform geographic analysis and create informative maps.
## Installation

The plugin can be downloaded from [the QGIS plugin repository](https://plugins.qgis.org/plugins/census_downloader/) or using the integrated plugin search functionality within QGIS itself (Plugins > Manage and Install Plugins > All > [Search 'Census Downloader']).
## Features
### Available Data Sets:
- ACS 1 Year
- ACS 5 Year
- Decennial Census

### Available Tables:
- Detail Table
- Subject Table
- Data Table
- Demographic and Housing Characteristics
- Demographic Profile

### Available Geographies:
- ZCTA
- Block Group
- Census Tract
- Township
- Urban Areas & Clusters
- Metropolitan Statistical Area
- Designated and Incorporated Place
- School District (elementary, secondary, and unified)

### Available Counties:
- Champaign
- Clark
- Coles
- Cumberland
- De Witt
- Douglass
- Edgar
- Macon
- Moultrie
- Piatt
- Shelby
- Vermilion
- Ford
- Iroquois

### Available Years:
- ACS 1: 2010 - 2019, 2021 - 2022
- ACS 5: 2014 - 2022
- Decennial: 2020

The availability of certain parameters is dependent on the state of other parameters. The plugin will automatically disable ineligible options in the dropdown menus.



## Documentation

- [Gitlab Wiki](https://gitlab.com/ccrpc/census_downloader/-/wikis/home)
- [CCRPC Wiki](https://wiki.ccrpc.org/doku.php?id=dt:gis:census-downloader-plugin)

## License

[GPL](https://choosealicense.com/licenses/gpl-3.0/)


## Authors

- [Amer Islam](mailto://aislam@ccrpc.org)


## Acknowledgements

 - Icon made by andinur from www.flaticon.com
 - [Awesome Readme Templates](https://awesomeopensource.com/project/elangosundar/awesome-README-templates)
 - [Awesome README](https://github.com/matiassingers/awesome-readme)
 - [How to write a Good readme](https://bulldogjob.com/news/449-how-to-write-a-good-readme-for-your-github-project)
